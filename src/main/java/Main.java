import java.util.*;

public class Main {
    public static void main(String[] args) {

        Double[] mass = {1.1, 2.2, 3.0, 4.4, 5.0};
        Array<Double> array = new Array<Double>(mass);
        array.swap();
        Arrays.asList(mass);
        

        Apple a1 = new Apple();
        Apple a2 = new Apple();
        Orange o1 = new Orange();
        Orange o2 = new Orange();

        Box appleBox = new Box();
        Box orangeBox = new Box();

        appleBox.putFruit(a1);
        appleBox.putFruit(a2);
        orangeBox.putFruit(o1);
        orangeBox.putFruit(o2);



        System.out.println(appleBox.getFruit());
        System.out.println(orangeBox.getFruit());


    }
}
