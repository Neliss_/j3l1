import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Array<T> {
    private T[] nums;

    public Array(T[] nums) {
        this.nums = nums;
    }

    public T[] getNums() {
        return nums;
    }

    public void setNums(T[] nums) {
        this.nums = nums;
    }

    public void swap() {
        for (int i = 0; i < nums.length; i++) {
            Number x = (Number) nums[0];
            nums[0] = nums[1];
            nums[1] = (T) x;

        }
        System.out.println(Arrays.toString(nums));
    }




}
